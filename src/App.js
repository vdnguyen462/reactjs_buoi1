import logo from './logo.svg';
import './App.css';
import Header from './BaiTapThucHanhLayout/Header';
import Body from './BaiTapThucHanhLayout/Body';
import Item from './BaiTapThucHanhLayout/Item';
import Footer from './BaiTapThucHanhLayout/Footer';

function App() {
  return (
    <div>
      <Header/>
      <Body/>
      <div className="container pb-5">
        <div class="row">
          <div class="col"><Item/></div>
          <div class="col"><Item/></div>
          <div class="col"><Item/></div>
          <div class="w-100"></div>
          <div class="col"><Item/></div>
          <div class="col"><Item/></div>
          <div class="col"><Item/></div>
        </div>
      </div>
      <Footer/>
    </div>
  );
}

export default App;
