import React, { Component } from 'react'
import "./Item.css"
export default class Item extends Component {
  render() {
    return (
      <div>
        <div className="container text-center pt-5">
            <div className="features bg-light pt-3">
                <div className="icon bg-primary text-white display-3 rounded-4 mb-4 mt-n5">
                    <i class="fa fa-download"></i>
                </div>
                <div className="content">
                    <h2 className='font-weight-bold'>Fresh new layout</h2>
                    <p className='p-3'>With Bootstrap 5, we've created a fresh new layout for this template! </p>
                </div>
            </div>
        </div>
      </div>
    )
  }
}
